#### 安装

```sh
composer require app-validate
```

#### 内置规则大全

| 规则              | 描述                 |
|-----------------|--------------------|
| required        | 必填                 |
| required-choose | 必选                 |
| safe            | 安全接收               |
| default         | 默认值                |
| string          | 字符串                |
| in              | 包含                 |
| exclude-in      | 禁选指定项目             |
| multi-selector  | 多选验证器              |
| identity-number | 身份证号               |
| url             | 网址                 |
| point           | 位置坐标               |
| email           | 邮箱                 |
| ip              | IP地址               |
| phone           | 手机号                |
| mobile          | 手机号                |
| list            | 列表                 |
| array           | 数组                 |
| match           | 正则表达式              |
| number          | 数字                 |
| int             | 整数                 |
| money           | 金额                 |
| qq              | QQ号                |
| datetime        | 日期时间 默认Y-m-d H:i:s |
| date            | 日期 默认Y-m-d         |
| date-format     | 时间字符串验证，正确就行,不验证格式 |
| list-intersect  | 列表交集               |
| json            | json字符串            |
| compare         | 一致性对比              |
| accepted        | 通过                 |
| zipcode         | 邮编                 |

#### 验证示例(class验证)

```php
declare(strict_types=1);

use Zlf\AppValidate\ValidateAbstract;
use Zlf\AppValidate\Type;
class ValidateData extends ValidateAbstract{

    /**
     * 验证规则
     */
    public function rules(): array
    {
        return [
            [['name','url'],Type::REQUIRED],
            ['name', Type::STRING],
            ['url', Type::URL],
        ];
    }

    /**
     * 字段描述
     */
    public function labels(): array
    {
        return [
            'name' => '名称',
            'url'=>'个人主页'
        ];
    }
}

//验证器实例
$validate = new ValidateData();
```

#### 验证示例(逻辑验证)

```php
<?php
declare(strict_types=1);
use Zlf\AppValidate\Validate

$validate = new Validate();
$validate->setRules([]);
$validate->setLabels([]);
$validate->setData([]);
```

#### 验证

```php
<?php
declare(strict_types=1);
$validate->validate();
if ($validate->isFail()) {//验证失败,获取错误信息
    print_r($validate->getErrorList());
} else {//验证成功,获取验证器数据
    print_r($validate->getSafeData());
}

```

#### 验证器实例方法

| 方法             | 描述           |
|----------------|--------------|
| validate       | 执行验证         |
| verifyMapping  | 验证器映射        |
| setData        | 设置验证数据       |
| getData        | 获取要验证的数据     |
| setLabels      | 设置字段描述       |
| setExclude     | 排除验证的字段      |
| setRules       | 设置验证规则       |
| setEmptySkip   | 不存在的字段是否跳过   |
| setNoSkipping  | 设置空值是否跳过     |
| setFields      | 设置要验证的字段     |
| setScene       | 验证场景         |
| where          | 验证条件         | 
| isFail         | 获取验证状态       |
| getErrors      | 获取错误信息       | 
| getErrorList   | 获取错误信息列表     |
| firstError     | 获取第一条错误信息    |
| finalError     | 获取最后一条错误信息   |
| addError       | 添加一条错误信息     |
| hasError       | 判断某字段是否有错误信息 |
| addErrors      | 添加错误信息       |
| setSafe        | 设置验证安全数据     |
| getSafeData    | 获取通过验证后的安全数据 |
| getSafe        | 获取指定安全验证数据   |
| beforeValidate | 验证前事件        |
| afterValidate  | 验证后事件        |

