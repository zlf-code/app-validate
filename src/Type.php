<?php
declare(strict_types=1);

namespace Zlf\AppValidate;

/**
 * REQUIRED 必填<br/>
 * REQUIRED_CHOOSE 必选<br/>
 * SAFE 安全接收<br/>
 * DEFAULT 默认值<br/>
 * STRING 字符串<br/>
 * IN 包含验证<br/>
 * EXCLUDE_IN 不能选择此项，和IN结果相反<br/>
 * IDENTITY_NUMBER 身份证号<br/>
 * URL 网址URL<br/>
 * POINT 坐标<br/>
 * EMAIL 电子邮件<br/>
 * IP IP地址<br/>
 * PHONE | MOBILE 手机号<br/>
 * LIST 列表<br/>
 * ARRAY 数组<br/>
 * MATCH 正则验证<br/>
 * NUMBER 数字验证<br/>
 * INT 整数验证<br/>
 * MONEY 金额验证<br/>
 * QQ QQ号验证<br/>
 * DATETIME 日期时间<br/>
 * DATE 日期<br/>
 * DATE_FORMAT 日期格式验证<br/>
 * LIST_INTERSECT 列表相交<br/>
 * JSON JSON字符串<br/>
 * COMPARE 对比<br/>
 * ACCEPTED 选择状态<br/>
 * ZIPCODE 邮政编码<br/>
 */
class Type
{
    /**
     * 限定数据必填<br/>
     * 'error' string 未填写时的错误信息
     */
    const REQUIRED = 'required';

    /**
     * 限定数据必选<br/>
     * 'error' string 未选择时的错误信息
     */
    const REQUIRED_CHOOSE = 'required-choose';

    /**
     * 代表数据是安全的,直接接收
     */
    const SAFE = 'safe';


    /**
     * 设置默认值
     * 'value' mixed 设置默认值
     */
    const DEFAULT = 'default';

    /**
     * 字符串类型数据验证<br/>
     * 'max' int 字符串最大长度<br/>
     * 'maxError' string 字符串最大长度验证失败提醒<br/>
     * 'min' int 字符串最少长度<br/>
     * 'minError' int 字符串最少长度错误验证提示<br/>
     * 'error' int 类型验证失败错误提示
     */
    const STRING = 'string';

    /**
     * 检查数据是否包含<br/>
     * -'enums' array [1,2,3,4]<br/>
     * -'error' string 错误信息<br/>
     * -'strict' bool 是否开启严格模式，开启后将同时验证类型<br/>
     */
    const IN = 'in';


    /**
     * 检查数据是否包含<br/>
     * -'enums' array [1,2,3,4]<br/>
     * -'error' string 错误信息<br/>
     * -'strict' bool 是否开启严格模式，开启后将同时验证类型<br/>
     */
    const EXCLUDE_IN = 'exclude-in';


    /**
     * 检查数据是否包含<br/>
     * -'enums' array [1,2,3,4]<br/>
     * -'error' string 错误信息<br/>
     * -'strict' bool 是否开启严格模式，开启后将同时验证类型<br/>
     * -'allowOther' bool 是否允许出现指定范围外的值<br/>
     * -'allowOtherError' string 出现指定范围外的值提示的错误<br/>
     * -'excludeInvalid' bool 是否排除无效值<br/>
     */
    const MULTI_SELECTOR = 'multi-selector';

    /**
     * 身份证号码验证<br/>
     * 'error' string 验证失败的错误信息
     */
    const IDENTITY_NUMBER = 'identity-number';

    /**
     * 验证一个网址是否合法<br/>
     * 'error' string 验证失败的错误信息
     */
    const URL = 'url';

    /**
     * 验证数据是否输一个经纬度 ["lng"=>105.123432,"lat"=>31.234212]<br/>
     * 'error' string 验证失败的错误信息
     */
    const POINT = 'point';

    /**
     * 验证数据是否是一个邮箱  12345@qq.com<br/>
     * 'error' string 验证失败的错误信息
     */
    const EMAIL = 'email';

    /**
     * IP验证<br />
     * 'error' string IP验证失败提醒<br />
     * 'type' string 指定验证IP类型  ipv4或ipv6<br />
     * 'typeError' string IP类型验证失败
     */
    const IP = 'ip';

    /**
     * 验证数据是否是一个手机号  19888887777<br/>
     * 'error' string 验证失败的错误信息
     */
    const PHONE = 'phone';

    /**
     * 验证数据是否是一个手机号  19888887777<br/>
     * 'error' string 验证失败的错误信息
     */
    const MOBILE = 'mobile';

    /**
     * 验证数据是否是一个list数据  [1,2,3]<br/>
     * 'error' string 类型验证失败提醒<br/>
     * 'type' string LIST元素类型  这次number和string<br/>
     * 'typeError' string 元素类型验证失败<br/>
     * 'count' int  指定元素数量<br/>
     * 'countError' string  指定元素数量验证失败<br/>
     * 'min' int  最少元素数量<br/>
     * 'minError' string  最少元素数量验证失败错误信息<br/>
     * 'max' int  最多元素数量<br/>
     * 'maxError' string  最多元素数量验证失败错误信息<br/>
     * 'enums' array|callback 判断数据元素是否属于此配置<br/>
     * 'enumsError' string 数据元素不合法的错误提示
     */
    const LIST = 'list';

    /**
     * 验证数据是否是一个数组  [1,2,3]<br/>
     * 'error' string 验证失败提醒
     */
    const ARRAY = 'array';

    /**
     * 正则验证器
     * 'pattern' string 正则表达式<br/>
     * 'not' bool 结果是否需要取反<br/>
     * 'error' string 验证失败信息
     */
    const MATCH = 'match';

    /**
     * 数字验证
     * 'error' string 数据类型未通过验证<br/>
     * 'max' int 数字最大值<br/>
     * 'maxError' string 数字最大值验证失败<br/>
     * 'min' int 数字最小值<br/>
     * 'minError' int 数字最小值验证失败<br/>
     * 'precision' int 数字精度<br/>
     * 'precisionError' string 数字精度验证未通过<br/>
     */
    const NUMBER = 'number';

    /**
     * 整数验证
     * 'error' string 数据类型未通过验证<br/>
     * 'max' int 数字最大值<br/>
     * 'maxError' string 数字最大值验证失败<br/>
     * 'min' int 数字最小值<br/>
     * 'minError' int 数字最小值验证失败<br/>
     */
    const INT = 'int';

    /**
     * 金额验证
     * 'error' string 数据类型未通过验证<br/>
     * 'max' int 数字最大值<br/>
     * 'maxError' string 数字最大值验证失败<br/>
     * 'min' int 数字最小值<br/>
     * 'minError' int 数字最小值验证失败<br/>
     */
    const MONEY = 'money';

    /**
     * QQ验证<br/>
     * 'error' string QQ未通过验证
     */
    const QQ = 'qq';

    /**
     * 时间日期验证器<br/>
     * 'error' string 时间日期验证未通过<br/>
     * 'format' string  时间日期格式<br/>
     * 'formatError' string  时间日期格式验证失败
     */
    const DATETIME = 'datetime';


    /**
     * 日期验证器<br/>
     * 'error' string 时间日期验证未通过<br/>
     * 'formatError' string  时间日期格式验证失败
     */
    const DATE = 'date';


    /**
     * 时间格式验证<br/>
     * 'error' string 时间日期验证未通过<br/>
     * 'output' string|null  输出格式 string为指定格式 null不更改<br/>
     * 'completion' string  输入时间格式，通过当前时间格式来补全时间
     * 'timestamp' bool  是否输出时间戳
     */
    const DATE_FORMAT = 'date-format';

    /**
     * 判断两个数组列表是否有交集<br/>
     * 'error' string 验证失败信息<br/>
     * ‘enums’ array|callback 验对比的数据
     */
    const LIST_INTERSECT = 'list-intersect';

    /**
     * JSON验证器<br/>
     * 'error' string 验证失败信息<br/>
     */
    const JSON = 'json';


    /**
     * 一致性对比<br/>
     * 'find' string 需要对比的字段<br/>
     * 'error' string '验证失败提示'
     */
    const COMPARE = 'compare';

    /**
     * 确认验证通过<br/>
     * 'enums' LIST ['ok','yes', 1, '1', true, 'true'] 以下值为通过值<br/>
     * 'error' string 验证失败值
     */
    const ACCEPTED = 'accepted';


    /**
     * 邮编验证<br/>
     * 'error' string 验证失败值
     */
    const ZIPCODE = 'zipcode';
}