<?php

declare(strict_types=1);

namespace Zlf\AppValidate;

/**
 * 类验证方式接口
 * Interface ValidateInterface
 * @package Components\Validate
 */
abstract class  ValidateAbstract extends Validate
{
    /**
     * 字段名称
     * @return array
     */
    public function labels(): array
    {
        return [];
    }


    abstract public function rules(): array;


    /**
     * @return $this
     */
    public function validate(): static
    {
        $this->setRules($this->rules())->setLabels($this->labels());
        return parent::validate();
    }
}