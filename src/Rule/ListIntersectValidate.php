<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;


use Zlf\AppException\Exception\ValidateException;
use Zlf\Unit\Is;

/**
 * 元素是否在数组中验证
 */
class ListIntersectValidate extends RuleAbstract
{
    /**
     * 枚举数据源
     * @var array
     */
    protected array $_enums = [];


    /**
     * 错误提示
     * @var string
     */
    protected string $_error = '{label}数据错误';


    /**
     * 严格模式 false过滤无效值  true时只能有效值
     * @var bool
     */
    protected bool $_strict = false;


    /**
     * 设置枚举数据
     * @param array|callable $enums
     * @return $this
     * @throws ValidateException
     */
    public function setEnums(array|callable $enums): ListIntersectValidate
    {
        if (gettype($enums) === 'object') {
            $enum_list = $enums();
        } else {
            $enum_list = $enums;
        }
        if (Is::list($enum_list)) {
            $this->_enums = $enum_list;
            return $this;
        }
        throw new ValidateException('验证参数enums错误');
    }


    /**
     * 严格模式
     * @param bool $strict
     * @return $this
     */
    public function setStrict(bool $strict): ListIntersectValidate
    {
        $this->_strict = $strict;
        return $this;
    }


    /**
     * 验证枚举
     * @param $value
     * @param string $label
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {
        if (Is::list($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            $new = [];
            $err = 0;
            foreach ($value as $val) {
                if (in_array($val, $this->_enums)) {
                    $new[] = $val;
                } else {
                    $err++;
                }
            }
            if ($this->_strict === true && $err > 0) {
                return $this->addError($this->_error, $label);
            }
            $this->_value = $new;
            return true;
        }
        return $this->addError($this->_error, $label);
    }
}