<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;


/**
 * 日期格式验证
 */
class DateFormatValidate extends RuleAbstract
{
    /**
     * 设置错误信息
     * @var string
     */
    protected string $_error = '{label}不是有效时间';


    /**
     * 时间格式验证
     * @var ?string
     */
    protected ?string $_output = null;


    /**
     * 是否输出时间戳
     * @var bool
     */
    protected bool $_timestamp = false;

    protected string $_completion = '';

    /**
     * 初始输出格式
     * @param string $output
     * @return $this
     */
    public function setOutput(string $output): static
    {
        $this->_output = $output;
        return $this;
    }

    /**
     * 是否输出时间戳
     * @param bool $timestamp
     * @return $this
     */
    public function setTimestamp(bool $timestamp): static
    {
        $this->_timestamp = $timestamp;
        return $this;
    }


    /**
     * 设置补全规则
     * @param bool $timestamp
     * @return $this
     */
    public function setCompletion(string $completion): static
    {
        $this->_completion = $completion;
        return $this;
    }

    /**
     * 验证日期时间
     * @param mixed $value 验证值
     * @param string $label 验证名
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {
        if (is_string($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = '';
                return true;
            }
            //验证时间格式是否正确
            $time = strtotime($value);
            if ($time === false) {
                return $this->addError($this->_error, $label);
            }

            //是否把时间补全
            if ($this->_completion) {
                $current = date($this->_completion, time());
                $val_len = strlen($value);
                $current_len = strlen($current);
                if ($current_len > $val_len) {
                    $value = substr_replace($current, $value, 0, $val_len);
                }
            }

            //是否时间转为时间戳
            if ($this->_timestamp === true) {
                $this->_value = $time;
                return true;
            }
            $this->_value = $this->_output ? date($this->_output, $time) : $value;
            return true;
        }
        return $this->addError($this->_error, $label);
    }

}