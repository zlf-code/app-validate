<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;


class UrlValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}不是有效的URL地址';


    public function validate($value, string $label = ''): bool
    {
        if (is_string($value) && strlen($value) < 2000) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            $pars = parse_url($value);
            if (isset($pars['scheme']) && isset($pars['host']) && (str_starts_with($pars['scheme'], 'https') || str_starts_with($pars['scheme'], 'http'))) {
                $this->_value = $value;
                return true;
            }
        }
        $this->addError($this->_error, $label);
        return false;
    }
}