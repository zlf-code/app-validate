<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

use Zlf\AppException\Exception\ValidateException;
use Zlf\Unit\Is;

/**
 * 列表验证器
 * Class ListValidate
 * @package Components\Validate\Rule
 */
class ListValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}未通过验证';

    protected string $_type = 'default';

    protected string $_typeError = '{label}不符合要求';


    protected int $_count = 0;
    protected string $_countError = '{label}数量只能是{count}个';


    protected int $_min = 0;
    protected string $_minError = '{label}不能少于{min}个';

    protected int $_max = 0;

    protected string $_maxError = '{label}不能超过{max}个';


    protected array $_enums = [];
    protected string $_enumsError = '{label}包含的选择项未通过验证';

    /**
     * 验证列表内部类型
     * @param string $type
     * @return ListValidate
     */
    public function setType(string $type): ListValidate
    {
        $this->_type = $type;
        return $this;
    }


    /**
     * 限制列表数据长度
     * @param int $count
     * @return ListValidate
     */
    public function setCount(int $count): ListValidate
    {
        $this->_count = $count;
        return $this;
    }


    /**
     * 限制列表数据长度错误信息
     * @param string $error
     * @return ListValidate
     */
    public function setCountError(string $error): ListValidate
    {
        $this->_countError = $error;
        return $this;
    }


    /**
     * 限制列表数据最大长度
     * @param int $max
     * @return ListValidate
     */
    public function setMax(int $max): ListValidate
    {
        $this->_max = $max;
        return $this;
    }


    /**
     * 限制列表数据最大长度错误信息
     * @param string $error
     * @return ListValidate
     */
    public function setMaxError(string $error): ListValidate
    {
        $this->_maxError = $error;
        return $this;
    }


    /**
     * 限制列表数据最小长度
     * @param int $min
     * @return ListValidate
     */
    public function setMin(int $min): ListValidate
    {
        $this->_min = $min;
        return $this;
    }


    /**
     * 限制列表最小数据长度错误信息
     * @param string $error
     * @return ListValidate
     */
    public function setMinError(string $error): ListValidate
    {
        $this->_minError = $error;
        return $this;
    }


    /**
     * 设置枚举数据
     * @param array|callable $enums
     * @return ListValidate
     * @throws ValidateException
     */
    public function setEnums(array|callable $enums): ListValidate
    {
        if (gettype($enums) === 'object') {
            $enum_list = $enums();
        } else {
            $enum_list = $enums;
        }
        if (Is::list($enum_list)) {
            $this->_enums = $enum_list;
            return $this;
        }
        throw new ValidateException('验证参数enums错误');
    }


    /**
     * 限制列表最小数据长度错误信息
     * @param string $error
     * @return ListValidate
     */
    public function setEnumsError(string $error): ListValidate
    {
        $this->_enumsError = $error;
        return $this;
    }


    public function validate($value, string $label = ''): bool
    {
        if ((is_array($value) && Is::list($value))) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            $valueCount = count($value);
            if ($this->_count > 0) {
                if ($valueCount != $this->_count) {
                    $this->addError($this->_countError, $label);
                }
            }
            if ($this->_min > 0) {
                if ($valueCount < $this->_min) {
                    $this->addError($this->_minError, $label);
                }
            }
            if ($this->_max > 0) {
                if (count($value) > $this->_max) {
                    $this->addError($this->_maxError, $label);
                }
            }
            if ($this->_type !== 'default') {
                $this->checkTypeList($value, $label);
            }

            if (count($this->_enums) > 0) {
                $intersect = array_intersect($this->_enums, $value);
                if (count($intersect) < count($value)) {
                    $this->addError($this->_enumsError, $label);
                }
            }

            if ($this->isFail()) {
                return false;
            }


            $this->_value = $value;
            return true;
        }
        return $this->addError($this->_error, $label);
    }

    /**
     * 验证list数据类型
     */
    private function checkTypeList(array $value, string $label): void
    {
        if ($this->_type === 'number') {
            foreach ($value as $val) {
                if (!is_numeric($val)) {
                    $this->addError($this->_typeError, $label);
                    break;
                }
            }
        } else if ($this->_type === 'string') {
            foreach ($value as $val) {
                if (!is_string($val)) {
                    $this->addError($this->_typeError, $label);
                    break;
                }
            }
        }
    }
}