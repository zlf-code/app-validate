<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

/**
 * 元素是否在数组中验证
 */
class AcceptedValidate extends RuleAbstract
{

    public array $_enums = ['ok', 'yes', 1, '1', true, 'true'];


    /**
     * 错误提示
     * @var string
     */
    protected string $_error = '请先确认{label}';


    /**
     * 设置枚举数据
     * @param array $enums
     * @return AcceptedValidate
     */
    public function setEnums(array $enums): static
    {
        $this->_enums = $enums;
        return $this;
    }


    /**
     * 验证枚举
     * @param $value
     * @param string $label
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {

        if (empty($value) && $this->_noSkipping === true) {
            $this->_value = $value;
            return true;
        }
        if (in_array($value, $this->_enums)) {
            $this->_value = $value;
            return true;
        }
        return $this->addError($this->_error, $label);
    }
}