<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

use Zlf\Unit\Is;

/**
 * JSON数据验证器
 */
class JsonValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}不是有效的数据格式';


    public function validate($value, string $label = ''): bool
    {
        if ($value && is_string($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            if (Is::json($value)) {
                $this->_value = $value;
                return true;
            }
        }
        $this->addError($this->_error, $label);
        return false;
    }
}