<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;


/**
 * 数组验证器
 * Class ListValidate
 * @package Components\Validate\Rule
 */
class ArrayValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}数据结构错误';


    public function validate($value, string $label = ''): bool
    {
        if (is_array($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = [];
                return true;
            }
            $this->_value = $value;
            return true;
        }
        return $this->addError($this->_error, $label);
    }
}