<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

use Zlf\Unit\Is;
use Zlf\Unit\Number;

class NumberValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}必须是数字';

    protected float|int|null $_min = null;
    protected string $_minError = "{label}不能小于{min}";


    protected int|float|null $_max = null;
    protected string $_maxError = "{label}不能大于{max}";


    protected int|null $_precision = null;
    protected string $_precisionError = "{label}只能精确到{precision}位小数";


    /**
     * 限制数字最大值
     * @param int|float $max
     * @return $this
     */
    public function setMax(float|int $max): NumberValidate
    {
        $this->_max = $max;
        return $this;
    }


    /**
     * 限制数字最大值错误提示
     * @param string $maxError
     * @return $this
     */
    public function setMaxError(string $maxError): NumberValidate
    {
        $this->_maxError = $maxError;
        return $this;
    }


    /**
     * 限制数字最小值
     * @param int|float $min
     * @return $this
     */
    public function setMin(float|int $min): NumberValidate
    {
        $this->_min = $min;
        return $this;
    }


    /**
     * 限制数字最小值错误提示
     * @param string $minError
     * @return $this
     */
    public function setMinError(string $minError): NumberValidate
    {
        $this->_minError = $minError;
        return $this;
    }


    /**
     * 设置精度
     * @param int $precision
     * @return $this
     */
    public function setPrecision(int $precision): NumberValidate
    {
        $this->_precision = $precision;
        return $this;
    }


    /**
     * 设置进度错误提示
     * @param string $precisionError
     * @return $this
     */
    public function setPrecisionError(string $precisionError): NumberValidate
    {
        $this->_precisionError = $precisionError;
        return $this;
    }


    /**
     * 开始验证
     * @param $value
     * @param string $label
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {
        if (is_numeric($value)) {
            if (Is::empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            $num = Number::format($value);
            if (is_int($this->_min) || is_float($this->_min)) {
                if ($num < $this->_min) {
                    $this->addError($this->_minError, $label);
                }
            }
            if (is_int($this->_max) || is_float($this->_max)) {
                if ($num > $this->_max) {
                    $this->addError($this->_maxError, $label);
                }
            }
            if (is_numeric($this->_precision) && Number::precision($num) > $this->_precision) {
                $this->addError($this->_precisionError, $label);
            }
            if ($this->isFail()) {
                return false;
            }
            $this->_value = $value;
            return true;
        } elseif (is_string($value) && empty($value)) {
            if ($this->_noSkipping) {
                $this->_value = $value;
                return true;
            }
        }
        $this->addError($this->_error, $label);
        return false;
    }
}