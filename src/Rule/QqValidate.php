<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

class QqValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}不是有效的QQ';


    public function validate($value, string $label = ''): bool
    {
        if ($value && is_string($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            if (preg_match('/^[1-9][0-9]{4,12}$/', $value)) {
                $this->_value = $value;
                return true;
            }
        }
        $this->addError($this->_error, $label);
        return false;
    }
}