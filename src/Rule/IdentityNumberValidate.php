<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

class IdentityNumberValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}错误';


    public function validate($value, string $label = ''): bool
    {
        if (is_string($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            if ($this->verifyIdCard($value)) {
                $this->_value = $value;
                return true;
            }
        }
        $this->addError($this->_error, $label);
        return false;
    }


    /**
     * 验证身份证
     * @param string $value
     * @return bool
     */
    private function verifyIdCard(string $value): bool
    {
        $pattern = '/^[1-9]\d{5}(((19|20)\d{2}(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|((19|20)\d{2}(0[469]|11)(0[1-9]|[12]\d|30))|(19|20)\d{2}02(0[1-9]|1\d|2[0-8]))\d{3}[\dxX]$/';
        if (!preg_match($pattern, $value)) {
            return false;
        }
        // 验证身份证号码的最后一位校验位是否正确
        $idcard_base = substr($value, 0, 17);
        $verify_code = substr($value, 17, 1);
        if (strtolower($verify_code) == 'x') {
            $verify_code = '10';
        }
        $factor = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
        $verify_code_list = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
        $total = 0;
        for ($i = 0; $i < 17; $i++) {
            $total += (int)substr($idcard_base, $i, 1) * $factor[$i];
        }
        $mod = $total % 11;
        if ($verify_code_list[$mod] == $verify_code) {
            return true;
        } else {
            return false;
        }
    }
}