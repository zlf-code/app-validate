<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;


class PhoneValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}不是有效的手机号码';


    public function validate($value, string $label = ''): bool
    {
        if (is_numeric($value) || is_string($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            if (preg_match('/^1[3-9]\d{9}$/', strval($value))) {
                $this->_value = $value;
                return true;
            }
        }
        $this->addError($this->_error, $label);
        return false;
    }
}