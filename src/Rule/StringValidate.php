<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

class StringValidate extends RuleAbstract
{

    /**
     * 最多字符数量
     * @var int
     */
    protected int $_max = 0;

    /**
     * 字符超出数量限制错误信息
     * @var string
     */
    protected string $_maxError = '{label}最多不能超过{max}个字';

    /**
     * 最少字符数量
     * @var int
     */
    protected int $_min = 0;

    /**
     * 字符超出数量限制错误信息
     * @var string
     */
    protected string $_minError = '{label}不能少于{min}个字';


    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}不是有效的string类型数据';


    /**
     * 设置最长字符
     * @param int $max
     * @return StringValidate
     */
    public function setMax(int $max): StringValidate
    {
        $this->_max = $max;
        return $this;
    }


    /**
     * 设置超出允许字数错误信息
     * @param string $_maxError
     * @return StringValidate
     */
    public function setMaxError(string $_maxError): StringValidate
    {
        $this->_maxError = $_maxError;
        return $this;
    }


    /**
     * 设置最小字符
     * @param int $min
     * @return StringValidate
     */
    public function setMin(int $min): StringValidate
    {
        $this->_min = $min;
        return $this;
    }


    /**
     * 设置小于长度错误信息
     * @param string $_minError
     * @return StringValidate
     */
    public function setMinError(string $_minError): StringValidate
    {
        $this->_minError = $_minError;
        return $this;
    }


    public function validate($value, string $label = ''): bool
    {
        if (!is_string($value)) {
            return $this->addError($this->_error, $label);
        }
        if (empty($value) && $this->_noSkipping === true) {
            $this->_value = $value;
            return true;
        }
        $strLen = mb_strlen($value);
        if ($this->_min > 0) {
            if ($strLen < $this->_min) {
                $this->addError($this->_minError, $label);
            }
        }
        if ($this->_max > 0) {
            if ($strLen > $this->_max) {
                $this->addError($this->_maxError, $label);
            }
        }
        if ($this->isFail()) {
            return false;
        }
        $this->_value = $value;
        return true;
    }
}