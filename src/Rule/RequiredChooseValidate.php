<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

use Zlf\Unit\Is;

class RequiredChooseValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '请选择{label}';


    public function validate($value, string $label = ''): bool
    {
        if (Is::notEmpty($value)) {
            $this->_value = $value;
            return true;
        }
        $this->addError($this->_error, $label);
        return false;
    }
}