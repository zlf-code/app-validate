<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

class PointValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}不是有效的坐标';


    public function validate($value, string $label = ''): bool
    {
        if (!is_array($value)) {
            $this->addError($this->_error, $label);
            return false;
        }
        if (isset($value['lng']) && $value['lat']) {
            $lng = strval($value['lng']);
            $lat = strval($value['lat']);
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            if (preg_match('/^-?((\d|[1-9]\d|1[0-7]\d)(\.\d+)?|180(\.0+)?)$/', $lng) && preg_match('/^-?((\d|[1-8]\d)(\.\d+)?|90(\.0+)?)$/', $lat)) {
                $this->_value = $value;
                return true;
            }
        }
        $this->addError($this->_error, $label);
        return false;
    }
}