<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;


/**
 * 元素是否在数组中验证
 */
class InValidate extends RuleAbstract
{
    /**
     * 枚举数据源
     * @var array
     */
    protected array $_enums = [];


    /**
     * 错误提示
     * @var string
     */
    protected string $_error = '{label}选择错误';


    /**
     * 是否还要检查类型
     * @var bool
     */
    protected bool $_strict = false;//是否还要检查类型

    /**
     * 设置枚举数据
     * @param array $enums
     * @return $this
     */
    public function setEnums(array $enums): InValidate
    {
        $this->_enums = $enums;
        return $this;
    }


    /**
     * 是否还要价差类型
     * @param bool $strict
     * @return $this
     */
    public function setStrict(bool $strict): InValidate
    {
        $this->_strict = $strict;
        return $this;
    }


    /**
     * 验证枚举
     * @param $value
     * @param string $label
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {
        if (is_string($value) || is_numeric($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            if (in_array($value, $this->_enums, $this->_strict)) {
                $this->_value = $value;
                return true;
            }
        }
        return $this->addError($this->_error, $label);
    }
}