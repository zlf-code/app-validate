<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

class ZipcodeValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}不是有效的邮政编码';


    /**
     * 开始验证
     * @param $value
     * @param string $label
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {

        if (is_string($value) || is_numeric($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            if (preg_match('/^[1-9]\d{5}$/', strval($value))) {
                $this->_value = $value;
                return true;
            }
        }
        return $this->addError($this->_error, $label);
    }
}