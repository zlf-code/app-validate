<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;


/**
 * 日期时间验器
 */
class DatetimeValidate extends RuleAbstract
{

    /**
     * 设置错误信息
     * @var string
     */
    protected string $_error = '{label}不是有效时间';

    /**
     * 时间格式验证
     * @var string
     */
    protected string $_format = 'Y-m-d H:i:s';


    protected string $_formatError = '{label}时间格式不符合要求';


    /**
     * 验证日期时间
     * @param mixed $value 验证值
     * @param string $label 验证名
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {
        if (is_string($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = '';
                return true;
            }
            if (strlen($value) === 0) {
                $this->_value = $value;
                return true;
            }
            $time = strtotime($value);
            if ($time === false) {
                return $this->addError($this->_error, $label);
            }
            if ($value === date($this->_format, $time)) {
                $this->_value = $value;
                return true;
            }
            return $this->addError($this->_formatError, $label);
        }
        return $this->addError($this->_error, $label);
    }


    /**
     * 设置验证格式
     * @param string $format
     * @return $this
     */
    public function setFormat(string $format): DatetimeValidate
    {
        $this->_format = $format;
        return $this;
    }


    /**
     * 设置验证格式
     * @param string $error
     * @return $this
     */
    public function setFormatError(string $error): DatetimeValidate
    {
        $this->_formatError = $error;
        return $this;
    }
}