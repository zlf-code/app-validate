<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;


use Zlf\Unit\Arr;

abstract class RuleAbstract
{
    /**
     * @var array 错误信息
     */
    protected array $_errors = [];


    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error;

    /**
     *  验证值
     */
    protected mixed $_value;


    /**
     * 跳过验证
     * @var bool
     */
    protected bool $_noSkipping = false;


    /**
     * 验证器初始化
     * RuleAbstract constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        foreach ($config as $key => $value) {
            $method = 'set' . ucfirst($key);
            call_user_func_array([$this, $method], [$value]);
        }
    }


    /**
     * 直接调用规则验证
     * @param mixed $value
     * @param array $config
     * @param string $label
     * @return string|bool
     */
    public static function check(mixed $value, array $config = [], string $label = '内容'): string|bool
    {
        $validate = new static($config);
        if ($validate->validate($value, $label) === true) {
            return true;
        }
        return $validate->firstError();
    }


    abstract public function validate($value, string $label = ''): bool;


    /**
     * 设置错误信息
     * @param string $error
     * @return RuleAbstract
     * @author 竹林风@875384189 2022/6/2 11:54
     */
    public function setError(string $error): RuleAbstract
    {
        $this->_error = $error;
        return $this;
    }


    /**
     * 空值时是否跳过验证
     * @param bool $noSkipping
     * @return $this
     */
    public function setNoSkipping(bool $noSkipping): RuleAbstract
    {
        $this->_noSkipping = $noSkipping;
        return $this;
    }


    /**
     * 设置验证器错误信息
     * @param string $error
     * @param string $label
     * @return bool
     * @author 竹林风@875384189 2022/6/2 11:43
     */
    public function addError(string $error, string $label = ''): bool
    {
        if (preg_match_all('#{(.*?)}#', $error, $all)) {
            foreach ($all[1] as $key) {
                if ($key === 'label') {
                    $error = str_replace('{label}', $label, $error);
                } else {
                    $attr = '_' . $key;
                    $error = str_replace('{' . $key . '}', strval($this->$attr ?? ''), $error);
                }
            }
        }
        $this->_errors[] = $error;
        return false;
    }


    /**
     * 获取第一条错误信息
     * @author 竹林风@875384189 2022/6/2 11:46
     */
    public function firstError(): string
    {
        if (count($this->_errors) > 0) {
            return Arr::firstValue($this->_errors);
        }
        return '';
    }


    /**
     * 获取第最后一条错误信息
     * @author 竹林风@875384189 2022/6/2 11:46
     */
    public function finalError(): string
    {
        if (count($this->_errors) > 0) {
            return Arr::finalValue($this->_errors);
        }
        return '';
    }


    /**
     * 获取错误信息
     * @author 竹林风@875384189 2022/6/2 11:50
     */
    public function getErrors(): array
    {
        return $this->_errors;
    }


    /**
     * 获取验证状态
     * @return bool
     * @author 竹林风@875384189 2022/6/2 11:55
     */
    public function isFail(): bool
    {
        return count($this->_errors) > 0;
    }


    /**
     * 获取数据值
     * @author 竹林风@875384189 2022/6/2 11:50
     */
    public function getValue()
    {
        return $this->_value;
    }

}