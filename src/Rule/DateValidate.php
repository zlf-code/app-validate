<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;


/**
 * 日期时间验器
 */
class DateValidate extends DatetimeValidate
{
    /**
     * 时间格式验证
     * @var string
     */
    protected string $_format = 'Y-m-d';
}