<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

use Zlf\AppException\Exception\ValidateException;
/**
 * 正则验证器
 * Class MatchValidate
 * @package Components\Validate\Rule
 */
class MatchValidate extends RuleAbstract
{
    /**
     * 正则表达式
     * @var string
     */
    protected string $_pattern;

    /**
     * 结果取反
     * @var bool
     */
    protected bool $_not = false;

    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}验证失败';



    /**
     * 设置表达式
     * @param string $pattern
     * @return MatchValidate
     */
    public function setPattern(string $pattern): MatchValidate
    {
        $this->_pattern = $pattern;
        return $this;
    }

    /**
     * 结果是否取反
     * @param bool $not
     * @return MatchValidate
     */
    public function setNot(bool $not): MatchValidate
    {
        $this->_not = $not;
        return $this;
    }


    /**
     * @throws ValidateException
     */
    public function validate($value, string $label = ''): bool
    {
        if (is_string($value) || is_numeric($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            if (!$this->_pattern) {
                throw new ValidateException('请配置' . $label . '验证表达式');
            }
            $status = (bool)preg_match($this->_pattern, strval($value));
            if ($this->_not) {
                $status = !$status;
            }
            if ($status) {
                $this->_value = $value;
                return true;
            }
        }
        return $this->addError($this->_error, $label);

    }
}