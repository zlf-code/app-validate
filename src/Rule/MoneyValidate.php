<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

use Zlf\Unit\Is;
use Zlf\Unit\Number;

class MoneyValidate extends RuleAbstract
{
    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}不是有效的金额';

    protected float|int|null $_min = 0;
    protected string $_minError = "{label}不能小于{min}";


    protected int|float|null $_max = null;
    protected string $_maxError = "{label}不能大于{max}";


    /**
     * 限制数字最大值
     * @param int|float $max
     * @return $this
     */
    public function setMax(float|int $max): MoneyValidate
    {
        $this->_max = $max;
        return $this;
    }


    /**
     * 限制数字最大值错误提示
     * @param string $maxError
     * @return $this
     */
    public function setMaxError(string $maxError): MoneyValidate
    {
        $this->_maxError = $maxError;
        return $this;
    }


    /**
     * 限制数字最小值
     * @param int|float $min
     * @return $this
     */
    public function setMin(float|int $min): MoneyValidate
    {
        $this->_min = $min;
        return $this;
    }


    /**
     * 限制数字最小值错误提示
     * @param string $minError
     * @return $this
     */
    public function setMinError(string $minError): MoneyValidate
    {
        $this->_minError = $minError;
        return $this;
    }


    /**
     * 开始验证
     * @param $value
     * @param string $label
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {
        if (is_numeric($value)) {
            if (Is::empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            $num = Number::format($value);
            if (is_int($this->_min) || is_float($this->_min)) {
                if ($num < $this->_min) {
                    $this->addError($this->_minError, $label);
                }
            }
            if (is_int($this->_max) || is_float($this->_max)) {
                if ($num > $this->_max) {
                    $this->addError($this->_maxError, $label);
                }
            }
            if (Number::precision($num) > 2) {
                $this->addError("{label}最多精确到分", $label);
            }
            if ($this->isFail()) {
                return false;
            }
            $this->_value = $value;
            return true;
        } elseif (is_string($value) && empty($value)) {
            if ($this->_noSkipping) {
                $this->_value = $value;
                return true;
            }
        }
        $this->addError($this->_error, $label);
        return false;
    }
}