<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

use const FILTER_FLAG_IPV4;
use const FILTER_FLAG_IPV6;
use const FILTER_VALIDATE_IP;

/**
 * IP地址验证
 */
class IpValidate extends RuleAbstract
{
    /**
     * 设置错误信息
     * @var string
     */
    protected string $_error = '{label}不是有效的IP';


    /**
     * 验证IP地址的类型
     * @example ipv4  ipv6
     * @var string
     */
    protected string $_type;


    /**
     * IP地址的类型错误提示
     * @var string
     */
    protected string $_typeError = "{label}不是{type}类型IP地址";


    /**
     * 设置IP类型验证
     * @param string $type
     * @return $this
     */
    public function setType(string $type): IpValidate
    {
        $this->_type = strtolower($type);
        return $this;
    }


    /**
     * 设置IP类型验证错误信息
     * @param string $error
     * @return $this
     */
    public function setTypeError(string $error): IpValidate
    {
        $this->_typeError = $error;
        return $this;
    }

    /**
     * 验证IP地址
     * @param $value
     * @param string $label
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {
        if (is_string($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            switch ($this->_type) {
                case 'ipv4':
                    if (filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                        $this->_value = $value;
                        return true;
                    }
                    $this->addError($this->_typeError ?: '{label}不是有效的ipv4地址', $label);
                    break;
                case 'ipv6':
                    if (filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
                        $this->_value = $value;
                        return true;
                    }
                    $this->addError($this->_typeError ?: '{label}不是有效的ipv6地址', $label);
                    break;
                default:
                    if (filter_var($value, FILTER_VALIDATE_IP)) {
                        $this->_value = $value;
                        return true;
                    }
            }
        }
        return $this->addError($this->_error, $label);
    }
}