<?php

declare(strict_types=1);

namespace Zlf\AppValidate\Rule;

use const FILTER_VALIDATE_EMAIL;

/**
 * 电子邮件验证
 */
class EmailValidate extends RuleAbstract
{


    /**
     * 默认错误消息
     * @var string
     */
    protected string $_error = '{label}不是有效的邮箱地址';


    /**
     * 验证电子邮件
     * @param $value
     * @param string $label
     * @return bool
     */
    public function validate($value, string $label = ''): bool
    {
        if (is_string($value)) {
            if (empty($value) && $this->_noSkipping === true) {
                $this->_value = $value;
                return true;
            }
            $state = filter_var($value, FILTER_VALIDATE_EMAIL);
            $this->_value = $value;
            if ($state) return true;
        }
        $this->addError($this->_error, $label);
        return false;
    }
}