<?php
declare(strict_types=1);

use Zlf\AppValidate\Type;
use Zlf\AppValidate\ValidateAbstract;

include "../vendor/autoload.php";

class Validate extends ValidateAbstract
{
    public function rules(): array
    {
        return [
            [
                'type',
                Type::MULTI_SELECTOR,
                'enums' => ['12', '121', '23', '34'],
                'on' => ['created']
            ]
        ];
    }

    public function labels(): array
    {
        return [
            'type' => '类型',
        ];
    }
}

$validate = new Validate();
$validate->setData([
    'type' => [12, 13]
]);
$validate->validate();
if ($validate->isFail()) {//验证失败
    print_r($validate->getErrorList());
} else {//验证成功
    print_r($validate->getSafeData());
}